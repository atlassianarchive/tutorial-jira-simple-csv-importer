package com.example.plugins.tutorial.jira.csvimport;

import java.util.List;

/**
 * Helper class to convert issues from csv values
 */
public class Issue {

    private final String issueId;
    private final String summary;
    private final String priority;
    private final String assignee;
    private final String customField;
    private final String linkedIssueId;

    public Issue(List<String> values) {
        issueId = values.get(0);
        summary = values.get(1);
        priority = values.get(2);
        assignee = values.get(3);
        customField = values.get(4);
        linkedIssueId = values.get(5);
    }

    public String getIssueId() {
        return issueId;
    }

    public String getSummary() {
        return summary;
    }

    public String getPriority() {
        return priority;
    }

    public String getAssignee() {
        return assignee;
    }

    public String getCustomField() {
        return customField;
    }

    public String getLinkedIssueId() {
        return linkedIssueId;
    }
}
