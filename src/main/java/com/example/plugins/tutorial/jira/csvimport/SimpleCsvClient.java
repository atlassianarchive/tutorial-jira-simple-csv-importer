package com.example.plugins.tutorial.jira.csvimport;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * we assume data is in constant format
 * issueId,summary, priority, assignee, customField,linkedIssueId
 */
public class SimpleCsvClient {
    private List<Issue> internalIssues;

    public List<Issue> getInternalIssues() {
        return internalIssues;
    }

    public SimpleCsvClient(String filePath) {
        try {
            final FileInputStream input = new FileInputStream(filePath);
            try {
                final List<String> content = IOUtils.readLines(input);
                internalIssues = Lists.newArrayListWithExpectedSize(content.size());
                final Splitter splitter = Splitter.on(",").trimResults();
                for (int i = 0, contentSize = content.size(); i < contentSize; i++) {
                    final String issueLine = content.get(i);
                    final ArrayList<String> values = Lists.newArrayList(splitter.split(issueLine));
                    if (values.size() != 6) {
                        throw new RuntimeException("Invalid line " + (i + 1) + " " + issueLine + " should contain six values");
                    }
                    internalIssues.add(new Issue(values));
                }
            } finally {
                input.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
