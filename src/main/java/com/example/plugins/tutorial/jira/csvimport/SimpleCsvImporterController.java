package com.example.plugins.tutorial.jira.csvimport;

import com.atlassian.jira.plugins.importer.imports.importer.ImportDataBean;
import com.atlassian.jira.plugins.importer.imports.importer.JiraDataImporter;
import com.atlassian.jira.plugins.importer.web.*;
import com.example.plugins.tutorial.jira.csvimport.web.SimpleCsvSetupPage;
import com.google.common.collect.Lists;

import java.util.List;

public class SimpleCsvImporterController extends AbstractImporterController {

    public static final String IMPORT_CONFIG_BEAN = "com.example.plugins.tutorial.jira.google.csvimport.config";
    public static final String IMPORT_ID = "com.example.plugins.tutorial.jira.google.csvimport.import";
    private final ConfigFileHandler configFileHandler;

    public SimpleCsvImporterController(JiraDataImporter importer, ConfigFileHandler configFileHandler) {
        super(importer, IMPORT_CONFIG_BEAN, IMPORT_ID);
        this.configFileHandler = configFileHandler;
    }

    @Override
    public boolean createImportProcessBean(AbstractSetupPage abstractSetupPage) {
        if (abstractSetupPage.invalidInput()) {
            return false;
        }
        final SimpleCsvSetupPage setupPage = (SimpleCsvSetupPage) abstractSetupPage;
        final SimpleCsvConfigBean configBean = new SimpleCsvConfigBean(new SimpleCsvClient(setupPage.getFilePath()));
        final ImportProcessBean importProcessBean = new ImportProcessBean();
        if (!configFileHandler.populateFromConfigFile(setupPage, configBean)) {
            return false;
        }
        importProcessBean.setConfigBean(configBean);
        storeImportProcessBeanInSession(importProcessBean);
        return true;
    }

    @Override
    public ImportDataBean createDataBean() throws Exception {
        final SimpleCsvConfigBean configBean = getConfigBeanFromSession();
        return new SimpleCsvDataBean(configBean);
    }

    private SimpleCsvConfigBean getConfigBeanFromSession() {
        final ImportProcessBean importProcessBean = getImportProcessBeanFromSession();
        return importProcessBean != null ? (SimpleCsvConfigBean) importProcessBean.getConfigBean() : null;
    }

    @Override
    public List<String> getSteps() {
        return Lists.newArrayList(SimpleCsvSetupPage.class.getSimpleName(),
                ImporterProjectMappingsPage.class.getSimpleName(),
                ImporterCustomFieldsPage.class.getSimpleName(),
                ImporterFieldMappingsPage.class.getSimpleName(),
                ImporterValueMappingsPage.class.getSimpleName(),
                ImporterLinksPage.class.getSimpleName());
    }
}
