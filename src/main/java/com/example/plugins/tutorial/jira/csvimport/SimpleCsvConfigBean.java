package com.example.plugins.tutorial.jira.csvimport;

import com.atlassian.jira.plugins.importer.external.beans.ExternalCustomField;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingDefinition;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingDefinitionsFactory;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingHelper;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingHelperImpl;
import com.atlassian.jira.plugins.importer.imports.importer.AbstractConfigBean2;
import com.example.plugins.tutorial.jira.csvimport.mapping.PriorityValueMappingDefinition;
import com.google.common.collect.Lists;

import java.util.List;

public class SimpleCsvConfigBean extends AbstractConfigBean2 {

    private SimpleCsvClient csvClient;

    public SimpleCsvConfigBean(SimpleCsvClient csvClient) {
        this.csvClient = csvClient;
    }

    @Override
    public List<String> getExternalProjectNames() {
        return Lists.newArrayList("project");
    }

    @Override
    public List<ExternalCustomField> getCustomFields() {
        return Lists.newArrayList(ExternalCustomField.createText("cf", "custom field"));
    }

    @Override
    public List<String> getLinkNamesFromDb() {
        return Lists.newArrayList("link");
    }

    @Override
    public void initializeValueMappingHelper() {
        final ValueMappingDefinitionsFactory mappingDefinitionFactory = new ValueMappingDefinitionsFactory() {
            public List<ValueMappingDefinition> createMappingDefinitions(ValueMappingHelper valueMappingHelper) {
                final List<ValueMappingDefinition> mappings = Lists.newArrayList();
                mappings.add(new PriorityValueMappingDefinition(getCsvClient(), getConstantsManager()));
                return mappings;

            }
        };
        valueMappingHelper = new ValueMappingHelperImpl(getWorkflowSchemeManager(),
                getWorkflowManager(), mappingDefinitionFactory, getConstantsManager());
    }

    public SimpleCsvClient getCsvClient() {
        return csvClient;
    }
}
