package com.example.plugins.tutorial.jira.csvimport;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.plugins.importer.external.CustomFieldConstants;
import com.atlassian.jira.plugins.importer.external.beans.*;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingHelper;
import com.atlassian.jira.plugins.importer.imports.importer.AbstractDataBean;
import com.atlassian.jira.plugins.importer.imports.importer.ImportLogger;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class SimpleCsvDataBean extends AbstractDataBean<SimpleCsvConfigBean> {

    private final SimpleCsvClient csvClient;
    private final SimpleCsvConfigBean configBean;
    private final ValueMappingHelper valueMappingHelper;

    public SimpleCsvDataBean(SimpleCsvConfigBean configBean) {
        super(configBean);
        this.configBean = configBean;
        this.csvClient = configBean.getCsvClient();
        this.valueMappingHelper = configBean.getValueMappingHelper();
    }

    @Override
    public Set<ExternalUser> getRequiredUsers(Collection<ExternalProject> projects, ImportLogger importLogger) {
        return getAllUsers(importLogger);
    }

    @Override
    public Set<ExternalUser> getAllUsers(ImportLogger log) {
        return Sets.newHashSet(Iterables.transform(csvClient.getInternalIssues(), new Function<Issue, ExternalUser>() {
            @Override
            public ExternalUser apply(Issue from) {
                return new ExternalUser(from.getAssignee(), from.getAssignee());
            }
        }));
    }

    @Override
    public Set<ExternalProject> getAllProjects(ImportLogger log) {
        final ExternalProject project = new ExternalProject(
                configBean.getProjectName("project"),
                configBean.getProjectKey("project"));
        project.setExternalName("project");
        return Sets.newHashSet(project);
    }

    @Override
    public Iterator<ExternalIssue> getIssuesIterator(ExternalProject externalProject, ImportLogger importLogger) {
        return Iterables.transform(csvClient.getInternalIssues(), new Function<Issue, ExternalIssue>() {
            @Override
            public ExternalIssue apply(Issue from) {
                final ExternalIssue externalIssue = new ExternalIssue();
                final String priorityMappedValue = valueMappingHelper.getValueMapping("priority", from.getPriority());
                externalIssue.setPriority(StringUtils.isBlank(priorityMappedValue) ? from.getPriority() : priorityMappedValue);
                externalIssue.setExternalId(from.getIssueId());
                externalIssue.setSummary(from.getSummary());
                externalIssue.setAssignee(from.getAssignee());
                externalIssue.setExternalCustomFieldValues(Lists.newArrayList(
                        new ExternalCustomFieldValue(configBean.getFieldMapping("cf"), CustomFieldConstants.TEXT_FIELD_TYPE,
                                CustomFieldConstants.TEXT_FIELD_SEARCHER, from.getCustomField())));
                externalIssue.setStatus(String.valueOf(IssueFieldConstants.OPEN_STATUS_ID));
                externalIssue.setIssueType(IssueFieldConstants.BUG_TYPE);
                return externalIssue;
            }
        }).iterator();
    }

    @Override
    public Collection<ExternalLink> getLinks(ImportLogger log) {
        final List<ExternalLink> externalLinks = Lists.newArrayList();
        final String linkName = configBean.getLinkMapping("link");
        for (Issue issue : csvClient.getInternalIssues()) {
            if (StringUtils.isNotBlank(issue.getLinkedIssueId())) {
                externalLinks.add(new ExternalLink(linkName, issue.getIssueId(), issue.getLinkedIssueId()));
            }
        }
        return externalLinks;
    }

    @Override
    public long getTotalIssues(Set<ExternalProject> selectedProjects, ImportLogger log) {
        return csvClient.getInternalIssues().size();
    }

    @Override
    public String getUnusedUsersGroup() {
        return "simple_csv_import_unused";
    }

    @Override
    public void cleanUp() {

    }

    @Override
    public String getIssueKeyRegex() {
        return null;
    }

    @Override
    public Collection<ExternalVersion> getVersions(ExternalProject externalProject, ImportLogger importLogger) {
        return Collections.emptyList();
    }

    @Override
    public Collection<ExternalComponent> getComponents(ExternalProject externalProject, ImportLogger importLogger) {
        return Collections.emptyList();
    }
}
