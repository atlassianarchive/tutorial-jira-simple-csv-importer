package com.example.plugins.tutorial.jira.csvimport.web;

import com.atlassian.jira.plugins.importer.extensions.ImporterController;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.plugins.importer.web.ConfigFileHandler;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.example.plugins.tutorial.jira.csvimport.SimpleCsvClient;

public class SimpleCsvSetupPage extends AbstractSetupPage {
    private String filePath;
    private final ConfigFileHandler configFileHandler;

    public SimpleCsvSetupPage(UsageTrackingService usageTrackingService, WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor, ConfigFileHandler configFileHandler) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
        this.configFileHandler = configFileHandler;
    }

    @Override
    public String doDefault() throws Exception {
        if (!isAdministrator()) {
            return "denied";
        }
        final ImporterController controller = getController();
        if (controller == null) {
            return RESTART_NEEDED;
        }
        return INPUT;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final ImporterController controller = getController();
        if (controller == null) {
            return RESTART_NEEDED;
        }
        if (!isPreviousClicked() && !controller.createImportProcessBean(this)) {
            return INPUT;
        }
        return super.doExecute();
    }

    @Override
    protected void doValidation() {
        if (isPreviousClicked()) {
            return;
        }
        try {
            new SimpleCsvClient(filePath);
        } catch (RuntimeException e) {
            addError("filePath", e.getMessage());
        }
        super.doValidation();
        configFileHandler.verifyConfigFileParam(this);
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}

